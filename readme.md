# jumpstarter.io Portal Auth implementation for Drupal

## Requirements
The module currently requires:
* the variable module

## Installation
1. Download & install module
2. Go to /admin/config/system/variable and set Site Owner variable to the user to be logged in (or dynamically set the variable)

## Linking with jumpstarter
The module will listen on /portal-auth

## Authorship
Written and maintained by Victor Purolnik at http://www.framewrk.eu

Feel free to write me at vp@framewrk.eu